## Welcome to the Westtown ML INSTRUCTOR Codebook!
### The following is the file structure of this repository
* [root]
  * `code/`
    * `main.ipynb` — main lab code notebook
  * `data/`
    * `digits_test.csv` — testing data
    * `digits_train.csv` — training data
  * `images/`
    * `PixelArt/` — images created using pixel art online software (more detail in lab instructions)
      * `PA_2.png`
      * `PA_5.png`
      * `PA_7.png`
    * `TrueHandwriting/` — images created by taking picture of actual human handwriting (more detail in lab instructions)
      * `TH_3.png`
      * `TH_4.png`
      * `TH_7.png`
    * `Instructional/` — images to be used in the lab instructions
  * `instructions/`
    * `instructions.pdf` — instructions slides
  * `.gitignore` — specifies which types of files to ignore
  * `README.md` — this file
  * `requirements.txt` — not necessary to look at, but contains the dependencies for this project. If people want to use this code on a non-Colab system, you must manually install required packages with the command `pip install -r requirements.txt` (after `cd`'ing into the repository folder)

## Instructions (Copied Here)
  <p>Please download the PDF to view it: <a href="https://gitlab.com/Ben-Drucker/Hand-Recog-ML/-/raw/main/instructions/instructions.pdf?inline=false">Download PDF</a>.</p>
